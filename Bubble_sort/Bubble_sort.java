class Sorting{
	
	static int count = 0;
	public static void sorting_array(int arr[], int size){
		for(int i=0; i<size-1; i++){
			boolean flag = false;

			for(int j=0; j<size-1-i; j++){
				count++;

				//logic 

				if(arr[j]>arr[j+1]){
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;

					flag = true;
				}
			}
			if(flag==false){
				break;
			}
		}	
	}
	public static void show(int arr[], int size){
		for(int i=0; i<size; i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
	public static void main(String[] args){
		int arr[] = {6,1,1,3,2,3,4,5};
		int size = arr.length;
		
		sorting_array(arr,size);

		System.out.println("The array in ascending order");
		show(arr,size);

		System.out.println(count);
	}
}

